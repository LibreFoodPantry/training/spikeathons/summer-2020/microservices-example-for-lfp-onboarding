# POGIL Activities

## Plan for POGIL Activities:
1. Introduction to the Domain: HFOSS, LibreFoodPantry and Bear Necessities Market OrderModule
2. Installing Docker
3. Containerization with Docker
4. Software Architectures from Monolith to Microservices
5. Understanding REST API calls
6. Trying REST API calls using a tool (Insomnia, HTTPie, or Postman) with an example backend
7. Designing new REST API calls
8. Understanding example code to implement REST API calls
9. Understanding example persistence layer implemented with a document database
10. Making queries to the persistence layer in the document database.
11. Implementing backend new REST API calls
12. Understanding example code to implement a frontend that makes REST API calls
13. Designing new frontend components
14. Implementing new frontend components
