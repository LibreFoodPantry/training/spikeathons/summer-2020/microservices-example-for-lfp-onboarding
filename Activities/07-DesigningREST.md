# Designing REST API Calls

## Content Learning Objectives


_After completing this activity, students should be able to:_

*

## Process Skill Goals

_During the activity, students should make progress toward:_

*

## Team Roles

_Decide what role each of you will play for today. Choose a role that you have not played before, or recently. The goal should be to have all team members rotate through the roles on a regular basis to become comfortable with all the roles. If you have only three people, one should have two roles. If you have five people, two may share the same role. Record role assignments here._


<table>
  <tr>
   <td>
Manager
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Presenter
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Recorder
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Technician/
<br>
Reflector
   </td>
   <td>
   </td>
  </tr>
</table>

## Model 1: Designing New Endpoints

There are pretty clearly more endpoints that would be useful to have for this API.

One obvious one would a request to change first and last name information for an existing customer. (After all, Sue Storm became Sue Richards when she married Mr. Fantastic.)

Another obvious one would be a request to get all the customers at once.


### Questions (15 min)



12. Design a REST API request to get an array containing an array of customer objects, that includes all customers in the database.
    33. HTTP Method:
    34. Endpoint:
    35. Body (JSON format):
    36. Return (JSON format):
13. Design a REST API request to change the first name and lastname of a customer, when you know the customer number:
    37. HTTP Method:
    38. Endpoint:
    39. Body (JSON format):
    40. Return (JSON format):
14. If you still have time, come up with another API request:
    41. Describe what it does:
    42. What database “tables” does it use:
    43. HTTP Method:
    44. Endpoint:
    45. Body (JSON format):
    46. Return (JSON format):
---

&copy; 2020 Karl R. Wurst <karl@w-sts.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.