# Exploring REST API Calls

## Content Learning Objectives


_After completing this activity, students should be able to:_

*

## Process Skill Goals

_During the activity, students should make progress toward:_

*

## Team Roles

_Decide what role each of you will play for today. Choose a role that you have not played before, or recently. The goal should be to have all team members rotate through the roles on a regular basis to become comfortable with all the roles. If you have only three people, one should have two roles. If you have five people, two may share the same role. Record role assignments here._


<table>
  <tr>
   <td>
Manager
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Presenter
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Recorder
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Technician/
<br>
Reflector
   </td>
   <td>
   </td>
  </tr>
</table>

## Model 1: Running the Order System REST API Backend

I have written an implementation of the Order System REST API Backend.

Have your technician do the following:


### Questions (? min)

13. Clone the REST-API Order System from 
[https://gitlab.com/worcester/cs/kwurst/rest-api-order-system](https://gitlab.com/worcester/cs/kwurst/rest-api-order-system) 
14. Open a command/terminal window in the cloned project.
15. Build and run the backend server with the command: 
<code>./gradlew bootrun</code></strong>
    18. The server will be built, and begin running. It will not terminate on its own. When you are done with the server, terminate it with Control-C.

## Model 2: Using the Order System REST API

*   Populate the system to have the data in the tables as shown in Model 1 of the Introduction to Rest activity.
*   Use Insomnia or Postman to do so.
*   The base URL for the server is [http://localhost:8080](http://localhost:8080)
*   Add the appropriate endpoint to the base URL.
*   The bodies will all be JSON.


### Questions (20 min)

16. Install a REST API tool.
    19. Either Insomnia ([https://insomnia.rest/](https://insomnia.rest/)) or Postman ([https://www.getpostman.com/](https://www.getpostman.com/)).
    20. Both are free to use, but Insomnia is Open Source, Postman is not.
17. Use your chose REST API tool to explore the Order System REST API.
18. Record your HTTP requests here, with their bodies, and returns:
---

&copy; 2020 Karl R. Wurst <karl@w-sts.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.