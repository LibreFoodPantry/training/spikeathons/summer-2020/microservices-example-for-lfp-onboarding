# Exploring REST API Implementation

## Content Learning Objectives


_After completing this activity, students should be able to:_

*

## Process Skill Goals

_During the activity, students should make progress toward:_

*

## Team Roles

_Decide what role each of you will play for today. Choose a role that you have not played before, or recently. The goal should be to have all team members rotate through the roles on a regular basis to become comfortable with all the roles. If you have only three people, one should have two roles. If you have five people, two may share the same role. Record role assignments here._


<table>
  <tr>
   <td>
Manager
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Presenter
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Recorder
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Technician/
<br>
Reflector
   </td>
   <td>
   </td>
  </tr>
</table>

# Model 1: Traditional Classes

Have your technician open the REST API Order System project in an IDE.


## Questions (8 min)



1. How many classes are there in the project? List them.
2. Open the **<code>Customer.java</code></strong> file.
    1. What it the purpose of this class?
    2. Is there anything about this class that takes advantage of Spring Boot?
    3. Is there anything here that you are not used to?
3. Open the <strong><code>Product.java</code></strong> file.
    4. What it the purpose of this class?
    5. Is there anything about this class that takes advantage of Spring Boot?
    6. Is there anything here that you are not used to?
4. Open the <strong><code>Order.java</code></strong> file.
    7. What it the purpose of this class?
    8. Is there anything about this class that takes advantage of Spring Boot?
    9. Is there anything here that you are not used to?
5. Open the <strong><code>Line.java</code></strong> file.
    10. What it the purpose of this class?
    11. Is there anything about this class that takes advantage of Spring Boot?
    12. Is there anything here that you are not used to?
6. Open the <strong><code>Database.java</code></strong> file.
    13. What it the purpose of this class?
    14. Is there anything about this class that takes advantage of Spring Boot?
    15. Is there anything here that you are not used to?


# 


# Model 2: The REST API Controller Classes

The system uses the Spring Boot framework: [http://spring.io/projects/spring-boot](http://spring.io/projects/spring-boot)

This framework makes it easy to create a REST API backend server. It provides most of the code for REST calls, you just need to add the “business logic” to use it the way you want it to.


## Questions (20 min)



7. Open the**<code> Application.java</code></strong> file.
    16. What makes it a Spring Application?
    17. What is the purpose of this class?
8. Open the <strong><code>CustomerController.java</code></strong> file.
    18. What makes it a Rest Controller?
    19. How many REST API endpoints does this class handle? 
        1. What are they? 
        2. What HTTP method does each handle?
        3. Give the line numbers for the code that handles each.
        4. How do you know what endpoint is handled by each?
        5. How do you know what HTTP method the endpoint handles?
    20. For the method that handles a POST request
        6. Give an example of a POST request that this endpoint would handle.
            1. URL:
            2. Body:
            3. Return:
        7. How is the request body communicated to the method?
        8. What is the purpose of this method?
    21. For the method that handles a GET request
        9. Give an example of a GET request that this endpoint would handle.
            4. URL:
            5. Body:
            6. Return:
        10. How is the customer number communicated to the method?
        11. What is the purpose of the <strong><code>if</code></strong> statement?
            7. What happens if the test is true?
                1. What is the <strong><code>HttpStatus</code></strong>?
            8. What happens if the test is false?
                2. What is the <strong><code>HttpStatus</code></strong>?
        12. What is the <strong><code>ResponseEntity</code></strong> used for?
            9. Why is the return type of the method <strong><code>ResponseEntity<Object></code></strong>?
    22. Why does the <strong><code>addNewCustomer</code></strong> method return a <strong><code>long</code></strong>, and not a <strong><code>ResponseEntity</code></strong>?
9. Open the <strong><code>ProductController.java</code></strong> file.
    23. In what ways does <strong><code>ProductController.java</code></strong> differ from <strong><code>CustomerController.java</code></strong>?
    24. Are any of them significant (meaning doing something new)?
10. Open the <strong><code>OrderController.java</code></strong> file.
    25. How many REST API endpoints does this class handle? 
        13. What are they? 
        14. What HTTP method does each handle?
    26. For the <strong><code>addProductToOrder</code></strong> method
        15. Give an example of a PUT request that this endpoint would handle.
            10. URL:
            11. Body:
            12. Return:
        16. Why does this method have both a <strong><code>@PathVariable</code></strong> and a <strong><code>@RequestBody</code></strong>?
        17. What is the purpose of this method?
    27. For the <strong><code>removeProductFromOrder</code></strong> method
        18. Give an example of a DELETE request that this endpoint would handle.
            13. URL:
            14. Body:
            15. Return:
        19. Why does are there three ways to <strong><code>return</code></strong> from this method?
            16. What does the first does the first <strong><code>return</code></strong> return?
                3. What is the <strong><code>HttpStatus</code></strong>?
                4. What are the conditions to get here?
            17. What does the first does the second <strong><code>return</code></strong> return?
                5. What is the <strong><code>HttpStatus</code></strong>?
                6. What are the conditions to get here?
            18. What does the first does the second <strong><code>return</code></strong> return?
                7. What is the <strong><code>HttpStatus</code></strong>?
                8. What are the conditions to get here?
        20. What is the purpose of this method?
11. Open the <strong><code>ShutdownController.java</code></strong> file.
    28. What is the endpoint used to shut down the server?
    29. What is the method that implements the endpoint?
    30. What HTTP method is used?
    31. Do you agree or disagree with the choice of HTTP method? Why or why not? If not, what method would you have chosen, and why?
    32. How does this Java method differ from the methods in the previous controllers? Why do you think this is?
    
---

&copy; 2020 Karl R. Wurst <karl@w-sts.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.