# Introduction to REST API Calls

## Content Learning Objectives


_After completing this activity, students should be able to:_

*

## Process Skill Goals

_During the activity, students should make progress toward:_

*

## Team Roles

_Decide what role each of you will play for today. Choose a role that you have not played before, or recently. The goal should be to have all team members rotate through the roles on a regular basis to become comfortable with all the roles. If you have only three people, one should have two roles. If you have five people, two may share the same role. Record role assignments here._


<table>
  <tr>
   <td>
Manager
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Presenter
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Recorder
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Technician/
<br>
Reflector
   </td>
   <td>
   </td>
  </tr>
</table>

## Model 1: OrderModule Data Schema

This model represents the data in the OrderModule. It stores information about individual orders and their status.

> Example data here showing a couple of placed orders and a couple of approvals. This is at the conceptual level. It does not need to be in json format.

### Questions (? min)

> Questions about the example data to encourage students to explore the data. Questions from old activity
> 1. What is Sue Storm’s customer number?
> 2. What product has the SKU of 2?
>     1. And what it its unit price?
> 3. What products did Johnny Storm order, and how many of each?
>    2. What is his order number?

## Model 2: REST APIs

> This Wikipedia information is old. Update?

### REST stands for Representational State Transfer. 

*   It is a Client-Server architecitecture that provides separation of concerns between a client that process and displays data, and a server that processes and stores data.
*   It is stateless, meaning that the server does not maintain any state for a particular client. The client maintains its own state, and any request it makes to the server contains any necessary context for the server to satisfy the request.
*   The REST API provides an uniform interface that can be accessed by a variety of clients.
*   Often implemented as a Web service API (called a RESTful API) and use the HTTP protocol using
    *   A base URL such as [http://api.example.com/resources](http://api.example.com/resources)
    *   A media type for transferring the data. JSON (Java Script Object Notation) is commonly used.
    *   Standard HTTP method such as GET, PUT, POST, DELETE.

<table>
  <tr>
   <td>
Uniform Resource Locator (URL)
   </td>
   <td>GET
   </td>
   <td>PUT
   </td>
   <td>PATCH
   </td>
   <td>POST
   </td>
   <td>DELETE
   </td>
  </tr>
  <tr>
   <td>Collection, such as https://api.example.com/resources/
   </td>
   <td><strong>List</strong> the URIs and perhaps other details of the collection's members. 
   </td>
   <td><strong>Replace</strong> the entire collection with another collection. 
   </td>
   <td>Not generally used 
   </td>
   <td><strong>Create</strong> a new entry in the collection. The new entry's URI is assigned automatically and is usually returned by the operation.
   </td>
   <td><strong>Delete</strong> the entire collection. 
   </td>
  </tr>
  <tr>
   <td>Element, such as https://api.example.com/resources/item17
   </td>
   <td><strong>Retrieve</strong> a representation of the addressed member of the collection, expressed in an appropriate Internet media type.
   </td>
   <td><strong>Replace</strong> the addressed member of the collection, or if it does not exist, <strong>create</strong> it. 
   </td>
   <td><strong>Update</strong> the addressed member of the collection. 
   </td>
   <td>Not generally used. Treat the addressed member as a collection in its own right and <strong>create</strong> a new entry within it
   </td>
   <td><strong>Delete</strong> the addressed member of the collection. 
   </td>
  </tr>
</table>

From [https://en.wikipedia.org/wiki/Representational_state_transfer](https://en.wikipedia.org/wiki/Representational_state_transfer)

### OrderModule REST API

> Include or link to OpenAPI docs.

### Questions (? min)

>Questions about the API to encourage students to explore the calls. Questions from old activity

>1. How many REST API endpoints are there for
> 3. Customer data
> 4. Product data
>5. Order data
>5. How many HTTP methods are used? List them.
>6. Based on the notes in table above, what is each of the HTTP methods used for?
>7. Based on the endpoints in the table above, what is represented by the italics in the endpoint?
>8. Based on the HTTP request bodies in the table above, what is represented by the italics in the body?
>9. Based on the HTTP return bodies in the table above, what is represented by the italics in the body?
>10. To add Ben Grimm to the customer database, what would your HTTP request look like:
>6. HTTP Method:
>7. Endpoint:
>8. Body:
>9. Return:
>11. To get the product information for Marshmallows from the product database, what would your HTTP request look like (you already know the product number (SKU) is 2):
>10. HTTP Method:
>11. Endpoint:
>12. Body:
>13. Return:
>12. To increase Johnny Storm’s order for Lighter Fluid by 6, what would your HTTP request look like (you already know his order number is 1 and the product number (SKU) for Lighter Fluid is 1):
>14. HTTP Method:
>15. Endpoint:
>16. Body:
>17. Return:

---

&copy; 2020 Karl R. Wurst <karl@w-sts.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.