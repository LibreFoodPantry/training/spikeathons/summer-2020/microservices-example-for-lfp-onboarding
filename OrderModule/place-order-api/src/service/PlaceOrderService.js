'use strict';

const MongoClient = require('mongodb').MongoClient
const ObjectID = require('mongodb').ObjectID

/**
 * Get a specific order
 *
 * id String id of order to return
 * returns OrderResponse
 **/
exports.getOrder = function(id) {
  return new Promise(
    async function(resolve, reject) {
      try {
        const mongoClient = await MongoClient.connect('mongodb://db_place_order');
        const db = await mongoClient.db('place-order');
        const ordersCollection = await db.collection('orders');
        const order = await ordersCollection.findOne({'_id': ObjectID(id)});
        // TODO: Throw 404 exception if id was not found.
        resolve(order);
      }
      catch (e) {
        console.log("getOrder failed:", e);
        reject(e.message);
      }
    }
  );
}


/**
 * List all orders
 *
 * returns List
 **/
exports.getOrders = function() {
  return new Promise(
    async function(resolve, reject) {
      try {
        const mongoClient = await MongoClient.connect('mongodb://db_place_order');
        const db = await mongoClient.db('place-order');
        const ordersCollection = await db.collection('orders');
        const ordersCursor = await ordersCollection.find();
        const ordersArray = await ordersCursor.toArray();
        resolve(ordersArray);
      }
      catch (e) {
        console.log("getOrders failed:", e);
        reject(e.message);
      }
    }
  );
}


/**
 * List the items that can be ordered
 *
 * returns inline_response_200
 **/
exports.listItems = function() {
  return new Promise(
    async function(resolve, reject) {
      try {
        const mongoClient = await MongoClient.connect('mongodb://db_place_order');
        const db = await mongoClient.db('place-order');
        const itemsCollection = await db.collection('items');
        const itemsCursor = await itemsCollection.find();
        const itemsArray = await itemsCursor.toArray();
        resolve(itemsArray);
      }
      catch (e) {
        console.log("listItems failed:", e);
        reject(e.message);
      }
    }
  );
}


/**
 * Place an order
 *
 * body Order  (optional)
 * no response value expected for this operation
 **/
exports.placeOrder = function(body) {
  return new Promise(
    async function(resolve, reject) {
      try {
        const mongoClient = await MongoClient.connect('mongodb://db_place_order');
        const db = await mongoClient.db('place-order');
        const ordersCollection = await db.collection('orders');
        // TODO: Validate body.
        const result = await ordersCollection.insertOne(body);
        resolve(result["ops"][0]);
      }
      catch (e) {
        console.log("placeOrder failed:", e);
        reject(e.message);
      }
    }
  );
}

