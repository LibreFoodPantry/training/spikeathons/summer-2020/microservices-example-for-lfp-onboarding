'use strict';

var utils = require('../utils/writer.js');
var PlaceOrder = require('../service/PlaceOrderService');

module.exports.getOrder = function getOrder (req, res, next, id) {
  PlaceOrder.getOrder(id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response, 500);
    });
};

module.exports.getOrders = function getOrders (req, res, next) {
  PlaceOrder.getOrders()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response, 500);
    });
};

module.exports.listItems = function listItems (req, res, next) {
  PlaceOrder.listItems()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response, 500);
    });
};

module.exports.placeOrder = function placeOrder (req, res, next, body) {
  PlaceOrder.placeOrder(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      // TODO: Handle 404s
      utils.writeJson(res, response, 500);
    });
};
