'use strict';

var utils = require('../utils/writer.js');
var ApproveOrder = require('../service/ApproveOrderService');

module.exports.approveOrder = function approveOrder (req, res, next, id) {
  ApproveOrder.approveOrder(id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getApprovedOrders = function getApprovedOrders (req, res, next) {
  ApproveOrder.getApprovedOrders()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getUnreviewedOrders = function getUnreviewedOrders (req, res, next) {
  ApproveOrder.getUnreviewedOrders()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.unapproveOrder = function unapproveOrder (req, res, next, id) {
  ApproveOrder.unapproveOrder(id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
