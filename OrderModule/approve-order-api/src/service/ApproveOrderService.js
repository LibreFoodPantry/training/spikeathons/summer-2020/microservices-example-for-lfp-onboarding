'use strict';
var mongo = require('mongodb');

/**
 * Approve an order
 *
 * id String id of order to approve
 * returns OrderResponse
 **/
exports.approveOrder = function(id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "preferences" : "preferences",
  "restrictions" : "restrictions",
  "id" : "id",
  "items" : [ "items", "items" ],
  "email" : "email"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

/**
 * List all approved orders
 *
 * returns List
 **/
exports.getApprovedOrders = function() {

  return new Promise(async function(resolve, reject) {
    var client = null; 
    var result = null;
    try {
      // Replace the uri string with your MongoDB deployment's connection string.
      const uri = "mongodb://db_approve_order";
      client = new mongo.MongoClient(uri, { useUnifiedTopology: true });
      await client.connect();
  
      const database = client.db('approve-order');
      const collection = database.collection('approvals');
  
      // Query for approved orders
      const query = { reviewStatus: 'approved' };
      const approval = await collection.find(query);
      result = approval.toArray();
      console.log(result);
    } finally {
      // Ensures that the client will close when you finish/error
      await client.close();
    }
    resolve(result);
  }).catch(error => {console.log('caught', error.message);});
}

/**
 * List all unreviewed orders
 *
 * returns List
 **/
exports.getUnreviewedOrders = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "preferences" : "preferences",
  "restrictions" : "restrictions",
  "id" : "id",
  "items" : [ "items", "items" ],
  "email" : "email"
}, {
  "preferences" : "preferences",
  "restrictions" : "restrictions",
  "id" : "id",
  "items" : [ "items", "items" ],
  "email" : "email"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * unapprove an order
 *
 * id String id of order to unapprove
 * returns OrderResponse
 **/
exports.unapproveOrder = function(id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "preferences" : "preferences",
  "restrictions" : "restrictions",
  "id" : "id",
  "items" : [ "items", "items" ],
  "email" : "email"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

